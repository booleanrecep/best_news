<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model as Eloquent;

class SavedNews extends Eloquent
{
    protected $table   = "saved_news";
}
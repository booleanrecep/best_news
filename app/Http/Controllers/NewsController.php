<?php

namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\SavedNews;
use Exception;

class NewsController extends Controller

{
 
    public function list(Request $request)
    {

        try {
            $country = $request->input('country');
            $category = $request->input('category');
            $source = $request->input('source');
            $keyword = $request->input('keyword');
            $date_at = $request->input('date_at');
            $where = [];

            if( $country && $country != '' ) {
                $where[] = "country = '$country'";
            }
            if( $category && $category != '' ) {
                $where[] = "( category = '$category' OR title LIKE '$category' OR description LIKE '$category' )";
            }
            if( $source && $source != '' ) {
                $where[] = "source = '$source'";
            }
            if( $keyword && $keyword != '' ) {
                if ( str_contains($keyword,',')  ) {
                    $k = "'".implode("','", explode(",",$keyword))."'";
                    $where[] = "( LOWER(title) IN ( $k ) OR LOWER(description) IN ( $k ) ) ";
                } else {
                    $where[] = "( LOWER(title) LIKE LOWER('%$keyword%') OR LOWER(description) LIKE LOWER('%$keyword%') ) ";

                }
            }
            if( $date_at && $date_at != '' ) {
                $where[] = "CONVERT(published_at, DATE) = '$date_at'";
            }
            if (count($where) > 0) {
                $where = "WHERE " . implode(' AND ', $where);
            } else {
                $where = '';
            }

            $user_id = Auth::check() ? Auth::user()->id : 'null';
            $news = app('db')->select("SELECT     
                id
                , title
                , description
                , published_at
                , category
                , source
                , country
                , author
                , url
                , img_url
                , content 
                , (SELECT COUNT(sn.id) FROM saved_news sn WHERE sn.title = news.title AND sn.user_id = $user_id ) AS is_saved
            FROM news
            $where
            LIMIT 100");

            return [
                'status'  => true,
                'data'    => array_chunk($news, 6),
            ];
        } catch (Exception $e) {
            return [
                'status'  => false,
                'message' => $e->getMessage(),
            ];
        }
    }

    public function save(Request $request)
    {
        try {
            if (SavedNews::where([['title',$request->title],['user_id',Auth::user()->id]])->count() > 0 ) {
                return [
                    'status'  => true,
                    'warning'  => true,
                    'message' => 'Already saved',
                ];
            }
            SavedNews::insert([...$request->input(), 'user_id' => Auth::user()->id ]);
            return [
                'status'  => true,
                'message' => 'Successfully saved',
            ];
        } catch (Exception $e) {
            return [
                'status'  => false,
                'message' => $e->getMessage(),
            ];
        }
    }
}

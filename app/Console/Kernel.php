<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Services\NewsApi;
use App\Services\Guardian;
use App\Services\Tagesschau;

class Kernel extends ConsoleKernel
{

    const TOPICS = [
        [ 'country' => 'us', 'category' => 'politic' ],
        [ 'country' => 'us', 'category' => 'business' ],
        [ 'country' => 'us', 'category' => 'economy' ],
        [ 'country' => 'tr', 'category' => 'politic'],
        [ 'country' => 'tr', 'category' => 'business'],
        [ 'country' => 'tr', 'category' => 'economy' ],
        [ 'country' => 'de', 'category' => 'politic' ],
        [ 'country' => 'de', 'category' => 'business' ],
        [ 'country' => 'de', 'category' => 'economy' ]

    ];
    /**
     * Define the application's command schedule.
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->call(fn () => (new Guardian())->getTopHeadlines())->everyThirtyMinutes();
        $schedule->call(fn () => (new Tagesschau())->getTopHeadlines())->everyThirtyMinutes();
        foreach (self::TOPICS as $item) {
            $schedule->call(fn () => (new NewsApi())->getTopHeadlines(''))->everySecond();
        }
    }

    /**
     * Register the commands for the application.
     */
    protected function commands(): void
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}

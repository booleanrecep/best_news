<?php

namespace App\Services;

use App\Models\News;
use Exception;

class NewsApi
{

    const NEWSAPI = [
        'url' => 'https://newsapi.org/v2/',
        'api_key' => 'e24a8d13bc404cc2bad0b4ee343b970f'
    ];

    public function getEverything(string $query = null, $from = null, $to = null, $sortBy = null)
    {
        try {
            $query  = $query ?? 'Apple';
            $from   = $from ?? date('Y-m-d', strtotime(date('Y-m-d') . ' - 1 days'));;
            $to     = $to ?? date('Y-m-d');
            $sortBy = $sortBy ?? 'popularity';

            $script =  "curl " . self::NEWSAPI['url'] . "/everything -G \
                -d q=" . $query . " \
                -d from=" . $from . " \
                -d from=" . $to . " \
                -d apiKey=" . self::NEWSAPI['api_key'];

            $res = shell_exec($script);
            $res = json_decode($res, true);
            if ($res['status']) {
                News::where('source', 'newsapi')->delete();
                sleep(1);
                $data = $res['articles'];
                $data = array_map(fn ($item) => [
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'content' => $item['content'],
                    'published_at' => $item['publishedAt'] ? date('Y-m-d H:i:s', strtotime($item['publishedAt'])) : null,
                    'category' => null,
                    'source' => 'newsapi',
                    'country' => null,
                    'author' => $item['author'],
                    'url' => $item['url'],
                    'img_url' => $item['urlToImage']
                ], $data);
                $chunk_size = 100;
                foreach (array_chunk($data, $chunk_size) as $data_chunk) {
                    News::insert($data_chunk);
                }
                return $data;
            }


            return 'Error when fetching';
        } catch (Exception $e) {
            custom_log($e->getMessage());
            return $e->getMessage();
        }
    }

    public function getTopHeadlines(string $country)
    {
        try {
            $country = $country ?? 'us';

            $script =  "curl " . self::NEWSAPI['url'] . "/top-headlines -G \
                -d country=" . $country . " \
                -d apiKey=" . self::NEWSAPI['api_key'];

            $res = shell_exec($script);
            $res = json_decode($res, true);
            if ($res['status']) {
                News::where([['source', 'newsapi'], ['country', "$country"]])->delete();
                sleep(1);
                $data = $res['articles'];
                $data = array_map(fn ($item) => [
                    'title' => $item['title'],
                    'description' => $item['description'],
                    'content' => $item['content'],
                    'published_at' => $item['publishedAt'] ? date('Y-m-d H:i:s', strtotime($item['publishedAt'])) : null,
                    'category' => '',
                    'source' => 'newsapi',
                    'country' => $country,
                    'author' => $item['author'],
                    'url' => $item['url'],
                    'img_url' => $item['urlToImage']
                ], $data);
                $chunk_size = 100;
                foreach (array_chunk($data, $chunk_size) as $data_chunk) {
                    News::insert($data_chunk);
                }
                return $data;
            }

            return 'Error when fetching';
        } catch (Exception $e) {
            custom_log($e->getMessage());
            return $e->getMessage();
        }
    }
}

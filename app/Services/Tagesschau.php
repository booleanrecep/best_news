<?php

namespace App\Services;

use App\Models\News;
use Exception;

class Tagesschau
{

    public function getTopHeadlines()
    {
        try {

            $script =  "curl -H 'Accept: application/json' -H 'Content-Type: application/json' -X GET https://www.tagesschau.de/api2u/news";

            $res = shell_exec($script);
            $res = json_decode($res, true);
            if ($res) {
                News::where([['source', 'tagesschau']])->delete();
                sleep(1);

                $data = $res['news'];
                $data = array_map(fn ($item) => [
                    'title' => $item['title'],
                    'description' => isset($item['detailsweb']) ? $item['detailsweb'] : '',
                    'content' => null,
                    'published_at' => $item['date'] ? date('Y-m-d H:i:s', strtotime($item['date'])) : null,
                    'category' => isset($item['tags'][0]) ? $item['tags'][0]['tag'] : '',
                    'source' => 'tagesschau',
                    'country' => 'de',
                    'author' => null,
                    'url' => isset($item['shareURL']) ? $item['shareURL'] : '',
                    'img_url' => isset($item['teaserImage']) ?  $item['teaserImage']['imageVariants']['1x1-432'] : ''
                ], $data);
                $chunk_size = 100;
                foreach (array_chunk($data, $chunk_size) as $data_chunk) {
                    News::insert($data_chunk);
                }
                return $data;
            }

            return 'Error when fetching';
        } catch (Exception $e) {
            custom_log($e->getMessage());
            return $e->getMessage();
        }
    }
}

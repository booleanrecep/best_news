<?php

namespace App\Services;

use App\Models\News;
use Exception;

class Guardian
{
    const API = [
        'url' => 'https://content.guardianapis.com',
        'api_key' => 'fc859a76-a4c0-410d-8230-12f74f7325a3'
    ];

    public function getTopHeadlines()
    {
        try {

            $script =  "curl " . self::API['url'] . "/search? -G \
                -d api-key=" . self::API['api_key'];

            $res = shell_exec($script);
            $res = json_decode($res, true);
            if ($res['response']['status']) {
                News::where([['source', 'guardian']])->delete();
                sleep(1);

                $data = $res['response']['results'];
                $data = array_map(fn ($item) => [
                    'title' => $item['webTitle'],
                    'description' => $item['apiUrl'],
                    'content' => null,
                    'published_at' => $item['webPublicationDate'] ? date('Y-m-d H:i:s', strtotime($item['webPublicationDate'])) : null,
                    'category' => $item['sectionName'],
                    'source' => 'guardian',
                    'country' => '',
                    'author' => null,
                    'url' => $item['webUrl'],
                    'img_url' => ''
                ], $data);
                $chunk_size = 100;
                foreach (array_chunk($data, $chunk_size) as $data_chunk) {
                    News::insert($data_chunk);
                }
                return $data;
            }

            return 'Error when fetching';
        } catch (Exception $e) {
            custom_log($e->getMessage());
            return $e->getMessage();
        }
    }
}

## Innoscripta Full Stack Task
![alt text](dashboard.png)

<h2>This Project is collecting data from those sources</h2>

> * [Tagesschaue](https://www.tagesschau.de/) - The Tagesschaue is a German daily newspaper

> * [Guardian](https://www.theguardian.com/) - The Guardian is a British daily newspaper

> * [NewsApi](https://newsapi.org/) - Free news api

## Project Tech Stack :
> * Laravel
> * Breeze
> * React
> * TailwindCss
> * MySQL

 Run locally
### Install and run dependencies consecutively in command line
```bash
composer install
npm install
php artisan serve
npm run dev # this should run in another terminal tab
```
### or simply in command line with Makefile
```bash
make run
```

 ### Run with Docker
 ```bash
docker-compose --env-file .env up --build
 ```

 NOTES  ⚠️ : 
> * The project has some issues for now. docker-compose is not well structered and mysql integration has some bugs so it can't be run with Docker.

> * I couldn't find a free Laravel hosting so I didn't deal with deploying ( my apoligizes )

> * `.env' file definitly should be .gitignored but to get rid of mysql installation and to let the app to test locally I have shared my MySQL credentials ( I have a shared hosting server for test purposes )

import { TiTick } from 'react-icons/ti';
import { MouseEvent } from 'react'
import moment from 'moment';
type T = {
  src: string
  title?: string
  url?: string
  desc?: string
  onClick?: (e: MouseEvent<HTMLElement>) => void
  disabled?: boolean
  is_saved?: boolean
  is_loggedin?: boolean,
  published_at?: Date
}

export const Card = ({
  src,
  title,
  desc,
  url = '',
  onClick,
  disabled = false,
  is_saved = false,
  is_loggedin,
  published_at
}: T) => {
  return title && (
    <div className='rounded overflow-hidden shadow-lg cursor-pointer'>
      <a href={url} target='_blank'>
        <img width={100} height={100} className='w-full' src={src || 'https://media.istockphoto.com/id/1468752191/tr/foto%C4%9Fraf/little-owl-athene-noctua-a-bird-stands-on-the-board-and-looks-away-placeholder-for-the-text.jpg?s=612x612&w=0&k=20&c=oUMJBrtlc9DahCOUQR0aA2tHv3HavkFD4nH2wyXbgZw='} alt='' />
        <div className='px-6 py-4'>
          {title ? <div className=' whitespace-nowrap font-bold text-xl mb-2 text-black'>{title}</div> : null}
          {desc ? <p className='text-gray-20 text-black'>{desc}</p> : null}
        </div>
      </a>
      <div className='px-6 pt-4 pb-2 justify-end flex'>
        <span className='text-gray-400 font-sans text-sm'>
          {published_at ? moment(published_at).format('DD/MM HH:mm') : ''}
        </span>
        {
          is_loggedin ?
            <button
              disabled={is_saved || disabled}
              onClick={onClick}
              className={`${is_saved ? 'bg-green-500 text-white' : 'bg-gray-200 text-green-700  '} flex items-center rounded-full px-3 py-1 text-sm font-semibold mr-2 mb-2`}>
              {is_saved ? <>Saved <TiTick /></> : 'Save'}
            </button>
            : null
        }
      </div>
    </div>

  )
}

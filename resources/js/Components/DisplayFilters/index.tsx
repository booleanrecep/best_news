import { Dispatch, SetStateAction, useCallback, useEffect, useState } from 'react'

type T = {
  filter: { [k: string]: string }
  setFilter: Dispatch<SetStateAction<any>>
  keysToHide?: string[]
  keyAsTitle?: null | ( (k: string) => string )
}
export const DisplayFilter = ({ filter, setFilter, keysToHide = [''], keyAsTitle = null }: T) => {

  return (
    <div className="flex">
          {Object.keys(filter).filter(item => !keysToHide.includes(item)).map((keyName) => (

            filter[keyName] && filter[keyName] != '' ?
              <div key={keyName} className='border-2 border-l-sky-700 border-dashed mr-2 p-1'>
                <span className=' text-blue-500 mr-1'>
                  <strong className=' text-gray-300'>{keyAsTitle ? keyAsTitle(keyName) : keyName.toUpperCase().replace("_", " ")}</strong> : {filter[keyName].toString()}
                </span>
                <button className="text-red-500" onClick={() => setFilter({ ...filter, [`${keyName}`]: '' })}>X</button>
              </div>
              : null
          ))}

    </div>
  )
}
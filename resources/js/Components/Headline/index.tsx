

type T = {
  text : string
}
export const Headline =({text} : T)=>{
  return(
    <h1 className="mb-4 text-4xl font-extrabold leading-none tracking-tight text-gray-900 md:text-5xl lg:text-6xl text-center py-1">{text}</h1>
  )
}
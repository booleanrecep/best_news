export type TNewsItem = {
  id: number,
  title: string,
  description: string
  published_at: Date,
  category: string,
  source: string,
  country: string,
  author: string,
  url: string,
  img_url: string
  content: string
  is_saved?: boolean
}
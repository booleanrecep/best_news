
import { Dispatch, SetStateAction, useCallback, useEffect, useState } from 'react'
import _ from 'lodash'

export const useDebouncedSearch = (setState : Dispatch<SetStateAction<any>>, property: string,minCharLenght:number=0 )=> {
  const [ search, setSearch ] = useState< any >('')

  const debouncedSearch = useCallback(
    _.debounce(
        (value : string | number | null ) => `${value}`.trim() == '' || `${value}`.length > minCharLenght ? setState((preS:any)=>({...preS, [property]: value})) : null,
        900
    ),
    []
  );

  useEffect(() => {
    debouncedSearch(search);
      return () => debouncedSearch.cancel();
    }, [search]);

    return [
      search,
      setSearch
    ]
}
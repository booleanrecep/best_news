export  const shortenText = (text: string,length: number = 100) => {
  let result: string = '';
  if (text) {
    result = text?.length < length ? text : text?.slice(0, length) + '...';
  }
  return result;
};

export const getURLSearchParams = (params : object) => {
  // @ts-ignore
  let newParams = new URLSearchParams(params)
  let keysForDel: any[] = []
  newParams.forEach((value, key) => {
    if (value == '' || value == null) {
      keysForDel.push(key)
    }
  })
  keysForDel.forEach((key) => {
    newParams.delete(key)
  })
  return newParams
}
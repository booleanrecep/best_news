import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link, router } from '@inertiajs/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import TextTransition, { presets } from 'react-text-transition';
import { PageProps } from '@/types';
import { toast } from 'react-toastify';
import { Card } from '@/Components';
import { shortenText } from '@/helpers';
import { TNewsItem } from '@/types/props';

type TData = {
    status: boolean
    data: TNewsItem[]
}

export default function List({auth}: PageProps) {

    const [data, setData] = useState<TData>({
        status: false,
        data: [],
    })
    const fetchData = async () => {

        const { data } = await axios.get(`/api/profile/list_news`)
        if (data.status) {
            setData(data)
        } else {
            toast.error('Error when fetching data!')
        }
    }

    const remove = async (id: number) => {
        const { data } = await axios.post('/api/profile/delete_news', {id : id})
        if (data.status) {
                toast.success(data.message)
                fetchData()
            } else {
            toast.error('Error when saving!')
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    const headers = data?.data?.flat().map((i) => ({ title: i.title, url: i.url }))
    return (
        <AuthenticatedLayout
            user={auth.user}
            header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">My News</h2>}

        >
            <Head title=">My News" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">

                    <div className='container mx-auto px-5 py-2 lg:px-16 lg:pt-12'>
                        <div className='-m-1 flex flex-wrap md:-m-2'>
                            <div className='flex flex-wrap'>
                                {
                                    data.data.map((item) =>

                                        <div className='md:w-1/4 sm:w-1/3 p-1 md:p-2' key={item.id}>
                                            <div className='rounded overflow-hidden shadow-lg cursor-pointer'>
                                                <a href={item.url} target='_blank'>
                                                    <img width={100} height={100} className='w-full' src={item.img_url} alt='' />
                                                    <div className='px-6 py-4'>
                                                        <div className='font-bold text-xl mb-2 text-black whitespace-nowrap'>{shortenText(item.title,15)}</div>
                                                        <p className='text-gray-20 text-black'>{shortenText(item.description,25)}</p>
                                                    </div>
                                                </a>
                                                <div className='px-6 pt-4 pb-2 justify-end flex'>
                                                    <button
                                                        onClick={() => remove(item.id)}
                                                        className={`bg-gray-200 text-red-600  flex items-center rounded-full px-3 py-1 text-sm font-semibold mr-2 mb-2`}>
                                                        Remove 
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </AuthenticatedLayout>
    );
}

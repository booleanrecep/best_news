import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head } from '@inertiajs/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import TextTransition, { presets } from 'react-text-transition';
import { PageProps } from '@/types';
import { toast } from 'react-toastify';
import { Card, DisplayFilter } from '@/Components';
import { getURLSearchParams, shortenText } from '@/helpers';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import { useDebouncedSearch } from '@/hooks';
import { TiBrush } from 'react-icons/ti';
import { TNewsItem } from '@/types/props';

type TData = {
    status: boolean
    data: TNewsItem[][]
}

type TFilterData = {
    keyword: string
    date_at: string
    category: string
    source: string
}

export default function Dashboard({ auth }: PageProps) {

    const [index, setIndex] = useState(0);

    useEffect(() => {
        const intervalId = setInterval(
            () => setIndex((index) => index + 1),
            4000,
        );
        return () => clearTimeout(intervalId);
    }, []);

    const [data, setData] = useState<TData>({
        status: false,
        data: [],
    })
    const initialFilterData: TFilterData = {
        source: '',
        category: '',
        keyword: '',
        date_at: '',
    }
    const [filterData, setFilterData] = useState<TFilterData>(initialFilterData)
    const [ searchKeyword, setSearchKeyword] = useDebouncedSearch(setFilterData,'keyword')
    const fetchData = async () => {
        const params = getURLSearchParams(filterData)
        const { data } = await axios.get(`/api/news/list?${params.toString()}`)
        if (data.status) {
            setData(data)
        } else {
            toast.error('Error when fetching data!')
        }
    }

    const saveToPreferences = async (news: TNewsItem) => {
        const newItem = Object.assign(news)
        delete newItem.is_saved
        delete newItem.id
        const { data } = await axios.post('/api/news/save', newItem)
        if (data.status) {
            if (data.warning) {
                toast.warning(data.message)
            } else {
                toast.success(data.message)
                fetchData()
            }
        } else {
            toast.error('Error when saving!')
        }
    }

    useEffect(() => {
        fetchData()
    }, [
        filterData.category,
        filterData.date_at,
        filterData.keyword,
        filterData.source,
    ])

    const headers = data?.data?.flat().map((i) => ({ title: i.title, url: i.url }))
    return (
        <AuthenticatedLayout
            user={auth.user}
        >
            <Head title="Dashboard" />

            <div className="py-6">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="md:flex md:items-center mb-6 gap-4">
                        <div className="md:w-1/1 sm:w-1/2">
                            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                                Keyword
                            </label>
                            <input
                                className=" bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#2563eb] " id="inline-full-name"
                                type="search"
                                value={searchKeyword}
                                onChange={(e) => setSearchKeyword( e.target.value )}
                            />
                        </div>
                        <div className="md:w-1/1 sm:w-1/2">
                            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                                Date
                            </label>
                            <DatePicker 
                                wrapperClassName=' bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#2563eb] " id="inline-full-name' 
                                className=' bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#2563eb] " id="inline-full-name' 
                                placeholderText='DD/MM/YYYYY'
                                selected={filterData.date_at ? new Date(filterData.date_at) : null } 
                                onChange={(date) => setFilterData({...filterData, date_at : date ? moment(date).format('YYYY-MM-DD') : ''})} 
                                />
                        </div>
                        <div className="md:w-1/1 sm:w-1/2">
                            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                                Category
                            </label>
                            <select
                                className=" bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#2563eb]"
                                value={filterData.category}
                                onChange={(e) => setFilterData({ ...filterData, category: e.target.value })}
                            >
                                {
                                    ['','business','economy','sport'].map((item) => <option key={item} value={item}>{item == '' ? 'All' : item}</option>)
                                }
                            </select>
                        </div>
                        <div className="md:w-1/1 sm:w-1/2">
                            <label className="block text-gray-500 font-bold md:text-left mb-1 md:mb-0 pr-4">
                                Source
                            </label>
                            <select
                                className=" bg-gray-200 appearance-none border-2 border-gray-200 rounded w-full py-2 px-4 text-gray-700 leading-tight focus:outline-none focus:bg-white focus:border-[#2563eb]"
                                value={filterData.source}
                                onChange={(e) => setFilterData({ ...filterData, source: e.target.value })}
                            >
                                {
                                    ['','guardian','tagesschau','newsapi'].map((item) => <option key={item} value={item}>{item == '' ? 'All' : item}</option>)
                                }
                            </select>
                        </div>
                        <div className="md:w-1/1 sm:w-1/2">
                            <label>&nbsp;</label>
                            <button onClick={()=>setFilterData(initialFilterData)} className='bg-blue-400 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded flex items-center'>
                            Clear <TiBrush />
                            </button>
                        </div>

                    </div>
                    <div className="md:flex md:items-center mb-6 gap-4">
                        <DisplayFilter setFilter={setFilterData} filter={filterData} />
                    </div>

                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg py-12">
                        <h1 className="pl-3 mb-4 font-extrabold leading-none tracking-tight text-gray-900 text-4xl text-center py-1">
                            <a href={headers[index]?.url} target='_blank'>
                                <TextTransition springConfig={presets.gentle}>{headers.map((i) => shortenText(i.title, 80))[index % headers.length]}</TextTransition>
                            </a>
                        </h1>

                    </div>

                    {
                        data.data.map((item: TNewsItem[]) =>
                            <div className='container mx-auto px-5 py-2 lg:px-16 lg:pt-12' key={item[0]?.id}>
                                <div className='-m-1 flex flex-wrap md:-m-2'>
                                    <div className='flex md:w-1/2 flex-wrap'>
                                        <div className='w-1/2 p-1 md:p-2'>
                                            <Card
                                                is_saved={item[0]?.is_saved}
                                                published_at={item[0]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[0]?.url}
                                                src={item[0]?.img_url}
                                                title={shortenText(item[0]?.title, 20)}
                                                desc={shortenText(item[0]?.description, 50)}
                                                onClick={() => saveToPreferences(item[0])}
                                            />
                                        </div>
                                        <div className='w-1/2 p-1 md:p-2'>
                                            <Card
                                                is_saved={item[1]?.is_saved}
                                                published_at={item[1]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[1]?.url}
                                                src={item[1]?.img_url}
                                                title={shortenText(item[1]?.title, 20)}
                                                desc={shortenText(item[1]?.description, 50)}
                                                onClick={() => saveToPreferences(item[1])}
                                            />
                                        </div>
                                        <div className='w-full p-1 md:p-2'>
                                            <Card
                                                is_saved={item[2]?.is_saved}
                                                published_at={item[2]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[2]?.url}
                                                src={item[2]?.img_url}
                                                title={shortenText(item[2]?.title, 20)}
                                                desc={shortenText(item[2]?.description, 50)}
                                                onClick={() => saveToPreferences(item[2])}
                                            />
                                        </div>
                                    </div>
                                    <div className='flex w-1/2 flex-wrap'>
                                        <div className='w-full p-1 md:p-2'>
                                            <Card
                                                is_saved={item[3]?.is_saved}
                                                published_at={item[3]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[3]?.url}
                                                src={item[3]?.img_url}
                                                title={shortenText(item[3]?.title, 20)}
                                                desc={shortenText(item[3]?.description, 50)}
                                                onClick={() => saveToPreferences(item[3])}
                                            />
                                        </div>
                                        <div className='w-1/2 p-1 md:p-2'>
                                            <Card
                                                is_saved={item[4]?.is_saved}
                                                published_at={item[4]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[4]?.url}
                                                src={item[4]?.img_url}
                                                title={shortenText(item[4]?.title, 20)}
                                                desc={shortenText(item[4]?.description, 50)}
                                                onClick={() => saveToPreferences(item[4])}
                                            />
                                        </div>
                                        <div className='w-1/2 p-1 md:p-2'>
                                            <Card
                                                is_saved={item[5]?.is_saved}
                                                published_at={item[5]?.published_at}
                                                is_loggedin={auth.user ? true : false}
                                                url={item[5]?.url}
                                                src={item[5]?.img_url}
                                                title={shortenText(item[5]?.title, 20)}
                                                desc={shortenText(item[5]?.description, 50)}
                                                onClick={() => saveToPreferences(item[5])}
                                            />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </div>
            </div>
        </AuthenticatedLayout>
    );
}

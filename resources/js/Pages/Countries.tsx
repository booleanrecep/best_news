import AuthenticatedLayout from '@/Layouts/AuthenticatedLayout';
import { Head, Link } from '@inertiajs/react';
import axios from 'axios';
import { useEffect, useState } from 'react';
import TextTransition, { presets } from 'react-text-transition';
import { PageProps } from '@/types';
import { toast } from 'react-toastify';
import { Card } from '@/Components';
import { getURLSearchParams, shortenText } from '@/helpers';
import { TNewsItem } from '@/types/props';

type TData = {
    status: boolean
    data: TNewsItem[][]
}

type TBtnActions = 'is_saving'

const COUNTRIES = [{ short: 'us', name: 'USA' }, { short: 'tr', name: 'Turkey' }, { short: 'de', name: 'Germany' }]
export default function Countries({ auth }: PageProps) {

    const countryFromUrl = new URLSearchParams(window.location.search).get('country') || 'tr'
    const [btnAction, setBtnAction] = useState<{ [k in TBtnActions]?: boolean }>({ })
    
    const [index, setIndex] = useState(0);
    useEffect(() => {
        const intervalId = setInterval(
            () => setIndex((index) => index + 1),
            4000,
        );
        return () => clearTimeout(intervalId);
    }, []);

    const [data, setData] = useState<TData>({
        status: false,
        data: [],
    })
    const headers = data?.data?.flat().map((i) => ({ title: i.title, url: i.url }))

    const fetchData = async () => {

        const params = getURLSearchParams({ country: countryFromUrl })
        const { data } = await axios.get(`/api/news/list?${params.toString()}`)
        if (data.status) {
            setData(data)
        } else {
            toast.error('Error when fetching data!')
        }
    }

    const saveToPreferences = async (news: TNewsItem) => {
        setBtnAction({is_saving : true})
        const newItem = Object.assign(news)
        delete newItem.is_saved
        delete newItem.id
        const { data } = await axios.post('/api/news/save', newItem)
        if (data.status) {
            if (data.warning) {
                toast.warning(data.message)
            } else {
                toast.success(data.message)
                fetchData()
            }
        } else {
            toast.error('Error when saving!')
        }
        setBtnAction({is_saving : false})

    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <AuthenticatedLayout
            user={auth.user}
            header={
                COUNTRIES.map((item) => <Link href={`countries?country=${item.short}`}><span className={`mx-2 ${countryFromUrl == item.short ? ' bg-gray-200 rounded-md p-1' : ''}`}>{item.name}</span></Link>)
            }
        >
            <Head title="Dashboard" />

            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg py-12">
                        <h1 className="pl-3 mb-4 font-extrabold leading-none tracking-tight text-gray-900 text-4xl text-center py-1">
                            {
                                headers.length > 0 ?
                                    <a href={headers[index]?.url} target='_blank'>
                                        <TextTransition springConfig={presets.gentle}>{headers.map((i) => shortenText(i.title, 80))[index % headers.length]}</TextTransition>
                                    </a>
                                    : <TextTransition springConfig={presets.gentle}>{['No news'][index % 1]}</TextTransition>

                            }
                        </h1>

                    </div>
                    {
                        data?.data?.length > 0 ?
                            data?.data?.map((item: TNewsItem[]) =>
                                <div className='container mx-auto px-5 py-2 lg:px-16 lg:pt-12' key={item[0]?.id}>
                                    <div className='-m-1 flex flex-wrap md:-m-2'>
                                        <div className='flex w-1/2 flex-wrap'>
                                            <div className='w-1/2 p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[0]?.is_saved}
                                                    url={item[0]?.url}
                                                    src={item[0]?.img_url}
                                                    title={item[0]?.title}
                                                    desc={shortenText(item[0]?.description)}
                                                    onClick={() => saveToPreferences(item[0])}
                                                />
                                            </div>
                                            <div className='w-1/2 p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[1]?.is_saved}
                                                    disabled={btnAction.is_saving}
                                                    url={item[1]?.url}
                                                    src={item[1]?.img_url}
                                                    title={item[1]?.title}
                                                    desc={shortenText(item[1]?.description)}
                                                    onClick={() => saveToPreferences(item[1])}
                                                />
                                            </div>
                                            <div className='w-full p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[2]?.is_saved}
                                                    disabled={btnAction.is_saving}
                                                    url={item[2]?.url}
                                                    src={item[2]?.img_url}
                                                    title={item[2]?.title}
                                                    desc={shortenText(item[2]?.description)}
                                                    onClick={() => saveToPreferences(item[2])}
                                                />
                                            </div>
                                        </div>
                                        <div className='flex w-1/2 flex-wrap'>
                                            <div className='w-full p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[3]?.is_saved}
                                                    disabled={btnAction.is_saving}
                                                    url={item[3]?.url}
                                                    src={item[3]?.img_url}
                                                    title={item[3]?.title}
                                                    desc={shortenText(item[3]?.description)}
                                                    onClick={() => saveToPreferences(item[3])}
                                                />
                                            </div>
                                            <div className='w-1/2 p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[4]?.is_saved}
                                                    disabled={btnAction.is_saving}
                                                    url={item[4]?.url}
                                                    src={item[4]?.img_url}
                                                    title={item[4]?.title}
                                                    desc={shortenText(item[4]?.description)}
                                                    onClick={() => saveToPreferences(item[4])}
                                                />
                                            </div>
                                            <div className='w-1/2 p-1 md:p-2'>
                                                <Card
                                                    is_saved={item[5]?.is_saved}
                                                    disabled={btnAction.is_saving}
                                                    url={item[5]?.url}
                                                    src={item[5]?.img_url}
                                                    title={item[5]?.title}
                                                    desc={shortenText(item[5]?.description)}
                                                    onClick={() => saveToPreferences(item[5])}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                            : <h2>No news at the moment</h2>
                    }
                </div>
            </div>
        </AuthenticatedLayout>
    );
}

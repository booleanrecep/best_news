<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Dashboard', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
});

Route::get('/dashboard', fn() => Inertia::render('Dashboard'))->name('dashboard');
Route::get('/test', fn()=> Inertia::render('Test'))->name('test');
Route::get('/world', fn()=> Inertia::render('World'))->name('world');
Route::get('/countries', fn()=> Inertia::render('Countries'))->name('countries');
Route::get('/sport', fn()=> Inertia::render('Sport'))->name('sport');
Route::get('/economy', fn()=> Inertia::render('Economy'))->name('economy');
// Route::get('/economy', fn()=> Inertia::render('Economy'))->middleware(['auth', 'verified'])->name('economy');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::get('/profile/list', [ProfileController::class, 'list'])->name('profile.list');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

require __DIR__.'/auth.php';

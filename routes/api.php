<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\ProfileController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::middleware('web')->get('/news/list',[NewsController::class,'list']);
Route::middleware('web')->post('/news/save',[NewsController::class,'save']);
Route::middleware('web')->post('/profile/delete_news',[ProfileController::class,'delete_news']);
Route::middleware('web')->get('/profile/list_news',[ProfileController::class,'list_news']);
Route::middleware('auth:sanctum')->get('/user', fn (Request $request)=> $request->user());

#!/bin/bash
this_file=$(basename "$0")

#give permission to this file
chmod +x $this_file

#install both backend and frontend dependencies
composer install
npm install

#laravel and react show be run in seperate process & ports
gnome-terminal -- php artisan serve
gnome-terminal -- npm run dev
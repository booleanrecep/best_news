
<?php

if (!function_exists('custom_log')) {
  function custom_log(string $message)
  {
    $myfile = fopen(basename(__FILE__, '.php') . '.log', "a+");
    fwrite($myfile, date('Y-m-d H:i:s') . " => " . $message . PHP_EOL);
    fclose($myfile);
  }
}
